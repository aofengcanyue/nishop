layui.use(['util'], function (util) {

    var
        table = util.table,//table
        layer = util.layer;//layer


    var orders = {

        constants: {
            html: {
                edit: appUrl + "/resources/js/lib/orders/edit.html"
            },
            api: {
                edit: appUrl + "/orders/edit",
                del: appUrl + "/orders/del",
                add: appUrl + "/orders/add",
                queryList: appUrl + '/orders/queryList'
            }
        },

        init: function () {
            this.renderPage()
        },

        renderPage: function () {
            var self = this;
            //执行一个 table 实例
            table.render({
                elem: '#ordersTable'
                , height: 800
                , url: self.constants.api.queryList//数据接口
                , method: 'post' //如果无需自定义HTTP类型，可不加该参数
                , title: '用户表'
                , page: true //开启分页
                , toolbar: 'default' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                , totalRow: true //开启合计行
                , parseData: function (resp) {
                    var data = resp.data || [];

                    resp.data = data.map(function (value) {

                        value.addTimeStr = util.getTime(value.addTime);

                        return value;
                    });
                    return resp;

                } //开启合计行
                , cols: [[ //表头
                    {type: 'checkbox', fixed: 'left'}
                    , {field: 'orderSn', title: '订单号', width: '15%'}
                    , {field: 'consignee', title: '下单人', width: '10%'}
                    , {field: 'mobile', title: '联系电话', width: '10%'}
                    , {field: 'goodsPrice', title: '实际支付', width: '10%'}
                    , {fixed: 'right', title: '商品信息', width: '10%', height: 315, align: 'center', templet: '#goodsInfo'}
                    , {fixed: 'right', title: '支付状态', width: '10%', align: 'center', templet: '#payStatus'}
                    , {fixed: 'right', title: '订单状态', width: '10%', align: 'center', templet: '#payStatus'}
                    , {field: 'addTimeStr', title: '下单时间', width: '10%'}
                    , {fixed: 'right', title: '操作', width: '10%', align: 'center', toolbar: '#ordersTableBar'}
                ]]
            });

            //监听头工具栏事件
            table.on('toolbar(ordersFilter)', function (obj) {
                var checkStatus = table.checkStatus(obj.config.id)
                    , data = checkStatus.data; //获取选中的数据
                switch (obj.event) {
                    case 'add':
                        self.add(obj.data || {});
                        break;
                    case 'update':
                        if (data.length === 0) {
                            layer.msg('请选择一行');
                        } else if (data.length > 1) {
                            layer.msg('只能同时编辑一个');
                        } else {
                            layer.alert('编辑 [id]：' + checkStatus.data[0].id);
                        }
                        break;
                    case 'delete':
                        if (data.length === 0) {
                            layer.msg('请选择一行');
                        } else {
                            layer.msg('删除');
                        }
                        break;
                }
                ;
            });

            //监听行工具事件
            table.on('tool(ordersFilter)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data //获得当前行数据
                    , layEvent = obj.event; //获得 lay-event 对应的值
                if (layEvent === 'detail') {
                    layer.msg('查看操作');
                } else if (layEvent === 'del') {
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构
                        layer.close(index);
                        self.del(data);
                    });
                } else if (layEvent === 'edit') {
                    self.edit(data);
                }
            });
        },
        edit: function (data) {
            var self = this;

            util.templateProxy(self.constants.html.edit, data,
                {
                    title: '编辑类目',
                    confirm: function (formData) {
                        util.post(self.constants.api.edit, formData, function (resp) {
                            if (resp && resp.success) {
                                layer.closeAll();
                                self.renderPage();
                            }
                        })
                    },
                    cancel: function () {
                        layer.closeAll();
                    }
                })
        },
        add: function (data) {
            var self = this;
            util.templateProxy(self.constants.html.edit, {
                    name: '',
                    enabled: true
                },
                {
                    title: '添加类目',
                    confirm: function (formData) {
                        util.post(self.constants.api.add, formData, function (resp) {
                            if (resp && resp.success) {
                                layer.closeAll();
                                self.renderPage();
                            }
                        })
                    },
                    cancel: function () {
                        layer.closeAll();
                    }
                })
        },
        del: function (data) {
            util.post(this.constants.api.del, {id: data.id}, function (resp) {
                layer.closeAll();
            });
        }

    };

    orders.init();
});