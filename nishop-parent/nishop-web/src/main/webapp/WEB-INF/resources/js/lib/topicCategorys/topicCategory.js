layui.use(['util'], function (util) {

    var
        table = util.table,//table
        layer = util.layer;//layer


    var topicCategory = {

        constants: {
            html: {
                edit:  appUrl +"/resources/js/lib/topicCategorys/edit.html"
            },
            api: {
                edit: appUrl + "/topicCategory/edit",
                del: appUrl + "/topicCategory/del",
                add: appUrl + "/topicCategory/add",
                querytopicCategory:appUrl + '/topicCategory/queryList'
            }
        },

        init: function () {
            this.renderPage()
        },

        renderPage: function () {
            var self = this;
            //执行一个 table 实例
            table.render({
                elem: '#topicCategoryTable'
                , height: 600
                , url: self.constants.api.querytopicCategory//数据接口
                , method: 'post' //如果无需自定义HTTP类型，可不加该参数
                , title: '用户表'
                , page: true //开启分页
                , toolbar: 'default' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                , totalRow: true //开启合计行
                , cols: [[ //表头
                    {type: 'checkbox', fixed: 'left'}
                    , {field: 'id', title: 'ID', width: '10%'}
                    , {field: 'title', title: '类别名称', width: '40%'}
                    , {fixed: 'right',title:'类别图标', width: '30%', align: 'center', templet: '<div><img src="{{d.picUrl}}" width="100" height="100"></div>'}
                    , {fixed: 'right',title:'操作', width: '20%', align: 'center', toolbar: '#topicCategoryTableBar'}
                ]]
            });

            //监听头工具栏事件
            table.on('toolbar(topicCategoryFilter)', function (obj) {
                var checkStatus = table.checkStatus(obj.config.id)
                    , data = checkStatus.data; //获取选中的数据
                switch (obj.event) {
                    case 'add':
                        self.add(obj.data || {});
                        break;
                    case 'update':
                        if (data.length === 0) {
                            layer.msg('请选择一行');
                        } else if (data.length > 1) {
                            layer.msg('只能同时编辑一个');
                        } else {
                            layer.alert('编辑 [id]：' + checkStatus.data[0].id);
                        }
                        break;
                    case 'delete':
                        if (data.length === 0) {
                            layer.msg('请选择一行');
                        } else {
                            layer.msg('删除');
                        }
                        break;
                }
                ;
            });

            //监听行工具事件
            table.on('tool(topicCategoryFilter)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data //获得当前行数据
                    , layEvent = obj.event; //获得 lay-event 对应的值
                if (layEvent === 'detail') {
                    layer.msg('查看操作');
                } else if (layEvent === 'del') {
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构
                        layer.close(index);
                        self.del(data);
                    });
                } else if (layEvent === 'edit') {
                    self.edit(data);
                }
            });
        },
        edit: function (data) {
            var self = this;

            util.templateProxy(self.constants.html.edit, data,
                {
                    title: '编辑类目',
                    confirm:function (formData) {
                        util.post(self.constants.api.edit, formData, function (resp) {
                            if (resp && resp.success) {
                                layer.closeAll();
                                self.renderPage();
                            }
                        })
                    },
                    cancel:function () {
                        layer.closeAll();
                    }
                })
        },
        add:function (data) {
            var self = this;
            util.templateProxy(self.constants.html.edit, {
                    name:'',
                    enabled:true
                },
                {
                    title: '添加类目',
                    confirm:function (formData) {
                        util.post(self.constants.api.add, formData, function (resp) {
                            if (resp && resp.success) {
                                layer.closeAll();
                                self.renderPage();
                            }
                        })
                    },
                    cancel:function () {
                        layer.closeAll();
                    }
                })
        },
        del:function (data) {
            util.post(this.constants.api.del, {id: data.id}, function (resp) {
                layer.closeAll();
            });
        }

    };

    topicCategory.init();
});