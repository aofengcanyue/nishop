layui.use(['util'], function (util) {

    var
        tree = util.tree,//tree
        upload = util.upload,//tree
        layer = util.layer;//layer


    var goodsAttrs = {

        constants: {
            html: {
                operator: appUrl + "/resources/js/lib/goodsAttrs/goodsOperator.html"
            },
            api: {
                subs: appUrl + "/goodsAttrs/subs",
                edit: appUrl + "/goodsAttrs/edit",
                del: appUrl + "/goodsAttrs/del",
                add: appUrl + "/goodsAttrs/add",
                queryLists: appUrl + '/goodsAttrs/queryLists'
            }
        },

        init: function () {
            this.renderTree();
            this.bindEvent();

        },
        bindEvent: function () {
            var self = this;
            var model = $('[data-model="goodsAttrs"]');
            model.off().on('click', '[action-name="add"]', function () {
                self.add({});
            }).on('click', '[action-name="del"]', function () {
                layer.confirm('真的删除行么', function () {
                    self.del({});
                });
            }).on('click', '[action-name="edit"]', function () {
                self.initUpload();
            });
        },
        renderTree: function () {
            var self = this;
            util.post(self.constants.api.queryLists, {}, function (resp) {
                if (resp && resp.data) {
                    $('#treeGoodsAttrs').empty();
                    tree({

                        elem: '#treeGoodsAttrs'
                        , nodes: resp.data || []
                        , click: function (node) {
                            self.detail(node);
                        }
                    });
                    self.detail(resp.data[0] || {});

                }
            })

        },

        initUpload: function () {
            var self = this, form = $('[data-model="goodsAttrs"]'),
                uploadId = '#uploadBannder';
            layer.closeAll();
            upload.render({
                elem: uploadId
                , url: self.constants.api.edit
                , accept: 'file' //普通文件
                , size: 200 //限制文件大小，单位 200kB
                , auto: false //关闭自动上传
                , data: util.getFormData(form) //关闭自动上传
                , bindAction: '[action-name="edit"]'
                , exts: 'jpg|png|jpeg' //只允许上传压缩文件
                , done: function (res) {
                    if (res && res.success) {
                        layer.msg(res.msg);
                        location.reload();
                    }
                }, choose: function (obj) {
                    obj.preview(function (index, file, result) {
                        $(uploadId).find('img').attr('src', result);
                    });
                }
            });
        },
        detail: function (node) {
            var self = this;
            self._cruuentNode = node;
            var html = util.getTemPlates(self.constants.html.operator);
            $('#goodsAttrsInfo').empty().append($.templates(html).render(node));
            self.initUpload();
        },
        add: function (data) {
            var self = this;
            util.templateProxy(self.constants.html.operator, data,
                {
                    title: '添加类目',
                    area: ['800px', '600px'],
                    confirm: function (formData, e) {
                        var form = $(e).find('form');
                        var _fromData = new FormData(form[0]);
                        util.send(self.constants.api.add, _fromData, {
                            processData: false,
                            cache: false,
                            contentType: false,
                            success: function (resp) {
                                if (resp && resp.success) {
                                    layer.msg(resp.msg);
                                    location.reload();
                                }
                            }
                        })
                    },
                    cancel: function () {
                        layer.closeAll();
                    }
                })
        },
        del: function (data) {
            util.post(this.constants.api.del, {id: data.id}, function () {
                layer.closeAll();
            });
        }
    };

    goodsAttrs.init();
});