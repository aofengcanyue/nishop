<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
	<head>
		<%@ include file="../public/public.jsp"%>
		<title>登录界面</title>
		<%
		String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
		%>
		<meta charset="utf-8">
		<title>Fullscreen Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- CSS -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
		<link rel="stylesheet" href="<%=basePath%>/resources/assets/css/reset.css">
		<link rel="stylesheet" href="<%=basePath%>/resources/assets/css/supersized.css">
		<link rel="stylesheet" href="<%=basePath%>/resources/assets/css/style.css">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body  id="loginBody">

		<div class="page-container">
			<h1>NISHOP</h1>
			<form method="post" action="<%=basePath%>/loginValid">
				<input type="text" name="username" class="username" placeholder="用户名称">
				<input type="password" name="password" class="password" placeholder="密码">
				<button type="submit">点击登陆</button>
				<div class="error"><span>+</span></div>
			</form>
			<div class="connect">
				<p>其他登陆方式:</p>
				<p>
					<a class="facebook" href="#"></a>
					<a class="twitter" href="#"></a>
				</p>
			</div>
		</div>
		<script>
			window.ctx = "<%=basePath%>";
		</script>
		<script src="<%=basePath%>/resources/assets/js/jquery-1.8.2.min.js"></script>
		<script src="<%=basePath%>/resources/assets/js/supersized.3.2.7.min.js"></script>
		<script src="<%=basePath%>/resources/assets/js/supersized-init.js"></script>
		<script src="<%=basePath%>/resources/assets/js/scripts.js"></script>
	</body>
</html>