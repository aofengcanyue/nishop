<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>类别管理</title>
</head>
<body>
<table class="layui-hide" id="attrsTable" lay-filter="attrsFilter"></table>
<script type="text/html" id="attrsTableBar">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="addSub">新增属性</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/html" id="attrsSubTableBar">
    <a class="layui-btn layui-btn-xs" lay-event="editSub">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delSub">删除</a>
</script>

<script type="text/html" id="attrsStatusBar">
  {{# if (d.enabled) { }}
  <div  class="ayui-bg-green">已启用</div>
  {{#  } else { }}

  <div  class="layui-bg-red">已禁用</div>
  {{#  } }}
</script>
<script type="text/html" id="attrsSubStatusBar">
    {{# if (d.inputType) { }}
    <div  class="ayui-bg-green">已启用</div>
    {{#  } else { }}
    <div  class="layui-bg-red">已禁用</div>
    {{#  } }}
</script>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/attrs/attrs.js"></script>

</body>
</html>