<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>内容管理</title>
</head>
<body>
<table class="layui-hide" id="topicsTable" lay-filter="topicsFilter"></table>
<script type="text/html" id="topicsTableBar">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>


<script type="text/html" id="showStatus">
    {{# if (d.isShow==true) { }}
    <div class="ayui-bg-green">展示中</div>
    {{#  } else { }}
    <div class="layui-bg-red">未展示</div>
    {{#  } }}
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/topics/topics.js"></script>


</body>
</html>