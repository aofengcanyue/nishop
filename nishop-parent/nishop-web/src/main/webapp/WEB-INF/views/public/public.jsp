<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/layui/css/layui.css"/>
    <%--<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/theme/css/bootstrap/bootstrap-theme.min.css"/>--%>
    <%--<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/theme/css/bootstrap/bootstrap.min.css"/>--%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/theme/css/base.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/theme/css/custom-styles.css"/>


<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/syslib/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/syslib/jsrender/jsrender.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/layui/layui.all.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/layui/expand/treeSelect.js"></script>
<%--util--%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/common/util.js"></script>


<script type="text/javascript">
	var appUrl = "<%=request.getContextPath()%>";
</script>
</head>
</html>