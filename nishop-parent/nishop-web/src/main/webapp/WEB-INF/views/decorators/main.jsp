<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <jsp:include page="../public/public.jsp"/>
    <title>
        <sitemesh:write property='title'/>
    </title>
</head>
<body class="" style="background: rgba(62, 62, 62, 0);">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">商城管理后台</div>
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item">
            <li class="layui-nav-item">
                <a href="#"> <i class="layui-icon layui-icon-speaker" style="color: yellow"></i>消息中心</a>
                <dl class="layui-nav-child">
                    <dd><a href="">统计中心</a></dd>
                    <dd><a href="#">待发货</a></dd>
                    <dd><a href="#">库存预警</a></dd>
                    <dd><a href="#">意见反馈</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="#">
                    我的
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a class="models" href="<%=request.getContextPath()%>/loginOut">退出登录</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-tab" lay-filter="demo">
            <ul class="layui-nav layui-nav-tree" lay-filter="mainNav" lay-shrink="all">
                <li class="layui-nav-item">
                    <a href="javascript:void(0);" class="parentModel">
                        <i class="layui-icon layui-icon-cart" style="color: #009688"></i>商品
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a class="models" href="<%=request.getContextPath()%>/advertise/index">
                            <i class="layui-icon layui-icon-picture">广告管理</i>
                        </a></dd>
                        <dd><a class="models" href="<%=request.getContextPath()%>/goods/index">
                            <i class="layui-icon layui-icon-cart-simple">商品管理</i></a></dd>
                        <dd><a class="models" href="<%=request.getContextPath()%>/goodsAttrs/index">
                            <i class="layui-icon layui-icon-note">商品类别</i></a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:void(0);" class="parentModel"> <i class="layui-icon layui-icon-set"
                                                                          style="color: #009688"></i>属性</a>
                    <dl class="layui-nav-child">
                        <dd><a class="models" href="<%=request.getContextPath()%>/attrs/index"> <i
                                class="layui-icon layui-icon-set-sm">商品属性</i></a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:void(0);" class="parentModel"><i class="layui-icon layui-icon-list"
                                                                         style="color: #009688"></i>订单</a>
                    <dl class="layui-nav-child">
                        <dd><a class="models" href="<%=request.getContextPath()%>/orders/index"><i
                                class="layui-icon layui-icon-form">订单管理</i></a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:void(0);" class="parentModel"><i
                            class="layui-icon layui-icon-release" style="color: #009688"></i>内容</a>
                    <dl class="layui-nav-child">
                        <dd><a class="models" href="<%=request.getContextPath()%>/topicCategory/index"><i
                                class="layui-icon layui-icon-note">文章分类</i></a></dd>
                        <dd><a class="models" href="<%=request.getContextPath()%>/topics/index"><i
                                class="layui-icon layui-icon-list">文章管理</i></a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <div class="layui-body">
        <div id="page-inner">
            <sitemesh:write property='body'/>
        </div>
    </div>
</div>
<script>
    layui.use('util', function (util) {
        var element = util.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        //初始化
        element.init();
        util.selected();
    });
</script>
</body>
</html>