<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>类别管理</title>
</head>
<body>
<table class="layui-hide" id="topicCategoryTable" lay-filter="topicCategoryFilter"></table>
<script type="text/html" id="topicCategoryTableBar">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

 

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/topicCategorys/topicCategory.js"></script>
 


</body>
</html>