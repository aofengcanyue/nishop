<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>商品分类</title>
</head>
<body>

<div class="layui-row " data-model="goodsAttrs">
    <blockquote class="layui-elem-quote ">商品分类
        <span style="margin-left: 1100px">
       <button class="layui-btn layui-btn-sm" action-name="add">
            <i class="right layui-icon layui-icon-add-1"></i>
       </button>
      <button class="layui-btn layui-btn-sm" action-name="edit">
            <i class="right layui-icon layui-icon-release"></i>
       </button>
        <button class="layui-btn layui-btn-sm layui-btn-danger" action-name="del">
            <i class="right layui-icon layui-icon-delete"></i>
        </button>
    </span>
    </blockquote>
    <div class="layui-col-md2">
        <div id="treeGoodsAttrs"
             style="display: inline-block; width: 100%; height: 800px; padding: 10px; border: 1px solid #ddd; overflow: auto;"></div>
    </div>
    <div class="layui-col-md10">

        <div id="goodsAttrsInfo">


        </div>

    </div>
</div>



<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/goodsAttrs/goodsAttrs.js"></script>


<script type="text/x-jsrender" id="editGoodsAttrsDivTmpl">


</script>


</body>
</html>