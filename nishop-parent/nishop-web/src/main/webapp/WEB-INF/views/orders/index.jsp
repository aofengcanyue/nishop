<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>订单管理</title>
</head>
<body>
<table class="layui-hide" id="ordersTable" lay-filter="ordersFilter"></table>
<script type="text/html" id="ordersTableBar">
    <a class="layui-btn layui-btn-xs" lay-event="info">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script type="text/html" id="payStatus">
    {{# if (d.orderStatus==0) { }}
    <div  class="ayui-bg-green">未付款</div>
    {{#  } else { }}

    <div  class="layui-bg-red">已付款</div>
    {{#  } }}
</script>
<script type="text/html" id="goodsInfo">
    {{#  layui.each(d.orderGoods, function(index, item){ }}
    <div class="layui-card">
        <div class="layui-card-header">商品单号:{{item.goodsSn}}</div>
        <div class="layui-card-body">
            <span>数量:{{item.number}}</span>
            <span>价格:{{item.retailPrice}}</span>
            <img  src="{{item.listPicUrl}}" width="100" height="100"/>
        </div>
    </div>

    {{#  }); }}
    {{#  if(d.orderGoods === 0){ }}
    <span>  无数据</span>
    {{#  } }}
</script>


<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/orders/orders.js"></script>
</body>
</html>