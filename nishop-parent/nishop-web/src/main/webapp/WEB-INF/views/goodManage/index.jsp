<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
	<head>
		<title>商品管理</title>
	</head>
	<body>
		<div class="layui-main-custom">
			<div class="layui-goods-search"> 
			  商品名称：
			  <div class="layui-inline">
			    <input class="layui-input" id="searchGoodsName" autocomplete="off">
			  </div>
			  <button class="layui-btn" data-type="reload" id="searchButton">搜索</button>
			</div>
		
			<table class="layui-hide" id="goodsTable" lay-filter="goodsTableBarFilter"></table>
 		</div>
		<script type="text/html" id="goodsTableBar">
  			<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  			<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
		</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/lib/goods/goods.js"></script>
	</body>
</html>