package com.nishop.web.controller;

import com.nishop.api.IOrdersService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.Order;
import com.nishop.api.form.BaseRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private IOrdersService iordersService;

    @RequestMapping("/index")
    public String index() {

        return "/orders/index";
    }


    @RequestMapping("/edit")
    @ResponseBody
    public ResponseBean<Boolean> edit(Order category) {
        boolean result = iordersService.edit(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/del")
    @ResponseBody
    public ResponseBean<Boolean> del(Order category) {
        boolean result = iordersService.del(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseBean<Boolean> add(Order category) {
        boolean result = iordersService.add(category);
        return ResponseBean.success(result, result ? "新增类别成功" : "新增类别失败");
    }

    /**
     * 查询类目列表
     *
     * @return
     */
    @RequestMapping("/queryList")
    @ResponseBody
    public LayuiTableResponse<Order> queryGoodsList(BaseRequestForm requestForm) {
        LayuiTableResponse<Order> result = new LayuiTableResponse<>();
        result.setData(iordersService.queryList(requestForm));
        result.setCount(iordersService.queryCount());
        return result;
    }
}
