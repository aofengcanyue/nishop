package com.nishop.web.controller;

import com.nishop.api.IAdvertiseService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.Advertise;
import com.nishop.api.form.BaseRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/advertise")
public class AdvertiseController {

    @Autowired
    private IAdvertiseService iadvertiseService;

    @RequestMapping("/index")
    public String index() {

        return "/advertise/index";
    }


    @RequestMapping("/edit")
    @ResponseBody
    public ResponseBean<Boolean> edit(Advertise category) {
        boolean result = iadvertiseService.edit(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/del")
    @ResponseBody
    public ResponseBean<Boolean> del(Advertise category) {
        boolean result = iadvertiseService.del(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseBean<Boolean> add(Advertise category) {
        boolean result = iadvertiseService.add(category);
        return ResponseBean.success(result, result ? "新增类别成功" : "新增类别失败");
    }

    /**
     * 查询类目列表
     *
     * @return
     */
    @RequestMapping("/queryList")
    @ResponseBody
    public LayuiTableResponse<Advertise> queryGoodsList(BaseRequestForm requestForm) {
        LayuiTableResponse<Advertise> result = new LayuiTableResponse<>();
        result.setData(iadvertiseService.queryList(requestForm));
        result.setCount(iadvertiseService.queryCount());
        return result;
    }
}
