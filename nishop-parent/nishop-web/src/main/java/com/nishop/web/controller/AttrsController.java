package com.nishop.web.controller;

import com.nishop.api.IAttrsService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.AttributeCategory;
import com.nishop.api.entity.GoodAttribute;
import com.nishop.api.form.BaseRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/attrs")
public class AttrsController {

    @Autowired
    private IAttrsService iAttrsService;

    @RequestMapping("/index")
    public String index() {

        return "/attrManage/index";
    }


    @RequestMapping("/edit")
    @ResponseBody
    public ResponseBean<Boolean> edit(AttributeCategory category) {
        boolean result = iAttrsService.edit(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/editSub")
    @ResponseBody
    public ResponseBean<Boolean> editSub(GoodAttribute attribute) {
        boolean result = iAttrsService.editSub(attribute);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/delSub")
    @ResponseBody
    public ResponseBean<Boolean> delSub(GoodAttribute attribute) {
        boolean result = iAttrsService.delSub(attribute);
        return ResponseBean.success(result, result ? "删除子属性成功" : "删除子属性失败");
    }

    @RequestMapping("/addSub")
    @ResponseBody
    public ResponseBean<Boolean> addSub(GoodAttribute attribute) {
        boolean result = iAttrsService.addSub(attribute);
        return ResponseBean.success(result, result ? "新增子类别成功" : "新增子类别失败");
    }

    @RequestMapping("/del")
    @ResponseBody
    public ResponseBean<Boolean> del(AttributeCategory category) {
        boolean result = iAttrsService.del(category);
        return ResponseBean.success(result, result ? "删除成功" : "删除失败");
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseBean<Boolean> add(AttributeCategory category) {
        boolean result = iAttrsService.add(category);
        return ResponseBean.success(result, result ? "新增类别成功" : "新增类别失败");
    }


    /**
     * 查询类目列表
     *
     * @return
     */
    @RequestMapping("/queryAttrsList")
    @ResponseBody
    public LayuiTableResponse<AttributeCategory> queryGoodsList(BaseRequestForm requestForm) {
        LayuiTableResponse<AttributeCategory> result = new LayuiTableResponse<AttributeCategory>();
        result.setData(iAttrsService.queryList(requestForm));
        result.setCount(iAttrsService.queryCount());
        return result;
    }
}
