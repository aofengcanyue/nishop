package com.nishop.web.controller;

import com.nishop.api.ITopicCategoryService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.TopicCategory;
import com.nishop.api.form.BaseRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/topicCategory")
public class TopicCategoryController {

    @Autowired
    private ITopicCategoryService itopicCategoryService;

    @RequestMapping("/index")
    public String index() {

        return "/topicCategory/index";
    }


    @RequestMapping("/edit")
    @ResponseBody
    public ResponseBean<Boolean> edit(TopicCategory category) {
        boolean result = itopicCategoryService.edit(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/del")
    @ResponseBody
    public ResponseBean<Boolean> del(TopicCategory category) {
        boolean result = itopicCategoryService.del(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseBean<Boolean> add(TopicCategory category) {
        boolean result = itopicCategoryService.add(category);
        return ResponseBean.success(result, result ? "新增类别成功" : "新增类别失败");
    }

    /**
     * 查询类目列表
     *
     * @return
     */
    @RequestMapping("/queryList")
    @ResponseBody
    public LayuiTableResponse<TopicCategory> queryGoodsList(BaseRequestForm requestForm) {
        LayuiTableResponse<TopicCategory> result = new LayuiTableResponse<TopicCategory>();
        result.setData(itopicCategoryService.queryList(requestForm));
        result.setCount(itopicCategoryService.queryCount());
        return result;
    }
}
