package com.nishop.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.nishop.api.IGoodsService;
import com.nishop.api.ITopicsService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.Topic;
import com.nishop.api.form.TopicsRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping("/topics")
public class TopicsController {

    @Autowired
    private ITopicsService iTopicsService;
    @Autowired
    private IGoodsService iGoodsService;
    @Autowired
    private HttpServletRequest request;

    @Value("${server.prefix:}")
    public String serverPrefix;


    @RequestMapping("/index")
    public String index() {

        return "/topicManage/index";
    }


    @RequestMapping("/edit")
    @ResponseBody
    public ResponseBean<Boolean> edit(Topic category) {


        String path = category.getItemPicUrl();
        category.setScenePicUrl(path);
        category.setAvatar(path);
        boolean result = iTopicsService.edit(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }

    @RequestMapping("/del")
    @ResponseBody
    public ResponseBean<Boolean> del(Topic category) {
        boolean result = iTopicsService.del(category);
        return ResponseBean.success(result, result ? "编辑成功" : "编辑失败");
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseBean<Boolean> add(Topic category) {
        boolean result = iTopicsService.add(category);
        return ResponseBean.success(result, result ? "新增类别成功" : "新增类别失败");
    }

    @RequestMapping("/uploadTopicsImage")
    @ResponseBody
    public JSONObject uploadGoodsImage(@RequestParam(name = "file") CommonsMultipartFile file)
            throws IOException {
        String filePath = iGoodsService.uploadGoodsImages(file);
        JSONObject s = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("src", filePath);
        data.put("title", "");
        s.put("code", 0);
        s.put("msg", "");
        s.put("data", data);
        return s;
    }

    /**
     * 查询类目列表
     *
     * @return
     */
    @RequestMapping("/queryList")
    @ResponseBody
    public LayuiTableResponse<Topic> queryGoodsList(TopicsRequestForm requestForm) {
        LayuiTableResponse<Topic> result = new LayuiTableResponse<Topic>();
        result.setData(iTopicsService.queryTopicList(requestForm));
        result.setCount(iTopicsService.queryCount());
        return result;
    }
}
