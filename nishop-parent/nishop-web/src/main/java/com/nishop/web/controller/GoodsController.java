package com.nishop.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.nishop.api.IGoodsService;
import com.nishop.api.common.LayuiTableResponse;
import com.nishop.api.common.NiShopConfig;
import com.nishop.api.common.ResponseBean;
import com.nishop.api.entity.Goods;
import com.nishop.api.form.GoodsRequestForm;
import com.nishop.api.layui.entity.TreeNode;
import com.nishop.api.util.FileUtil;

@Controller
@RequestMapping("/goods")
public class GoodsController {

	@Autowired
	private IGoodsService goodsService;

	@Autowired
	private NiShopConfig config;

	@RequestMapping("/index")
	public String index() {

		return "/goodManage/index";
	}

	/**
	 * 查询商品列表
	 * 
	 * @return
	 */
	@RequestMapping("/queryGoodsList")
	@ResponseBody
	public LayuiTableResponse<Goods> queryGoodsList(GoodsRequestForm goodsRequestForm) {
		LayuiTableResponse<Goods> result = new LayuiTableResponse<Goods>();
		result.setData(goodsService.queryGoodsList(goodsRequestForm));
		result.setCount(goodsService.queryGoodsCount(goodsRequestForm));
		return result;
	}

	@RequestMapping("/addGoods")
	@ResponseBody
	public ResponseBean<Boolean> addGoods(Goods goods) {
		boolean result = goodsService.addGoods(goods);
		return ResponseBean.success(result, result ? "新增商品成功" : "新增商品失败");
	}

	@RequestMapping("/editGoods")
	@ResponseBody
	public ResponseBean<Boolean> editGoods(Goods goods) {
		boolean result = goodsService.editGoods(goods);
		return ResponseBean.success(result, result ? "编辑商品成功" : "编辑商品失败");
	}

	@RequestMapping("/delGoods")
	@ResponseBody
	public ResponseBean<Boolean> delGoods(Goods goods) {
		boolean result = goodsService.delGoods(goods);
		return ResponseBean.success(result, result ? "删除商品成功" : "删除商品失败");
	}

	@RequestMapping("/uploadGoodsImage")
	@ResponseBody
	public JSONObject uploadGoodsImage(@RequestParam(name = "file") CommonsMultipartFile file)
			throws IOException {
		String filePath = goodsService.uploadGoodsImages(file);
		// layui 富文本需要的返回格式
		JSONObject result = new JSONObject();
		JSONObject data = new JSONObject();
		data.put("src", filePath);
		data.put("title", "");
		result.put("code", 0);
		result.put("msg", "");
		result.put("data", data);

		return result;
	}

	@RequestMapping("/getImageByUrl")
	public void getImageByUrl(String filePath, HttpServletRequest requert,
			HttpServletResponse response) throws IOException {
		String folderPath = FileUtil
				.getFileFullPath(config.goodsImagePath + File.separator + filePath);
		FileUtil.readFile(folderPath, response.getOutputStream());
	}

	@RequestMapping("/queryAttrTreeData")
	@ResponseBody
	public List<TreeNode> queryAttrTreeData(GoodsRequestForm goodsRequestForm) {
		return goodsService.queryAttrTreeData(goodsRequestForm);
	}
}
