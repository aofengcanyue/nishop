package com.nishop.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nishop.api.IloginService;
import com.nishop.api.common.NiShopConstant;
import com.nishop.api.entity.AdminUser;

@Controller
public class LoginController {

	@Autowired
	private IloginService loginService;

	@RequestMapping("/login")
	public String login() {
		return "/login/index";
	}

	@RequestMapping("/loginValid")
	public String loginValid(AdminUser user, HttpServletRequest request,
			HttpServletResponse response) {
		if (StringUtils.isNotBlank(user.getUsername())
				&& StringUtils.isNotBlank(user.getPassword())) {
			AdminUser loginUser = loginService.loginValid(user);

			if (loginUser == null) {
				return "redirect:/login";
			}
			request.getSession().setAttribute(NiShopConstant.LOGIN_TOKEN, loginUser);
			return "redirect:/goods/index";
		}
		return "redirect:/login";
	}

	@RequestMapping("/loginOut")
	public String loginOut(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		return "/login/index";
	}

}
