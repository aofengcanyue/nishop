/**
 *
 */
package com.nishop.service;

import com.nishop.api.ITopicCategoryService;
import com.nishop.api.entity.TopicCategory;
import com.nishop.api.entity.TopicCategoryExample;
import com.nishop.api.entity.TopicCategory;
import com.nishop.api.form.BaseRequestForm;
import com.nishop.api.util.Identity;
import com.nishop.service.dao.TopicCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author 李建成
 *
 */
@Service
public class TopicCategoryServiceImpl implements ITopicCategoryService {

    @Autowired
    private TopicCategoryMapper TopicCategoryMapper;

    @Override
    public List<TopicCategory> queryList(BaseRequestForm requestForm) {


        TopicCategoryExample example = getTopicCategoryExample();

        List<TopicCategory> attributeCategories = TopicCategoryMapper.selectByExample(example);

        return attributeCategories;
    }

    private TopicCategoryExample getTopicCategoryExample() {
        TopicCategoryExample example = new TopicCategoryExample();
        return example;
    }

    @Override
    public long queryCount() {

        TopicCategoryExample example = getTopicCategoryExample();

        long count = TopicCategoryMapper.countByExample(example);

        return count;
    }

    @Override
    public Boolean edit(TopicCategory category) {
        return TopicCategoryMapper.updateByPrimaryKeySelective(category) > 0;
    }

    @Override
    public Boolean add(TopicCategory category) {
        category.setId(Identity.uuidWithoutHyphen());
        return TopicCategoryMapper.insert(category) > 0;
    }

    @Override
    public Boolean del(TopicCategory category) {

        String id = category.getId();

        if (Objects.isNull(id)) {
            return false;
        }

        return TopicCategoryMapper.deleteByPrimaryKey(id) > 0;
    }
}
