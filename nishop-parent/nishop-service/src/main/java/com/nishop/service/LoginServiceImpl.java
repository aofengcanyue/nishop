package com.nishop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nishop.api.IloginService;
import com.nishop.api.entity.AdminUser;
import com.nishop.api.entity.AdminUserExample;
import com.nishop.service.dao.AdminUserMapper;

@Service
public class LoginServiceImpl implements IloginService {

	@Autowired
	private AdminUserMapper adminUserMapper;

	@Override
	public AdminUser loginValid(AdminUser user) {
		AdminUserExample example = new AdminUserExample();
		example.createCriteria().andUsernameEqualTo(user.getUsername())
				.andPasswordEqualTo(user.getPassword());
		List<AdminUser> users = adminUserMapper.selectByExample(example);
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

}
