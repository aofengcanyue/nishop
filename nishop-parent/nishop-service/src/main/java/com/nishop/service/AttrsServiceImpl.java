/**
 *
 */
package com.nishop.service;

import com.nishop.api.IAttrsService;
import com.nishop.api.entity.AttributeCategory;
import com.nishop.api.entity.AttributeCategoryExample;
import com.nishop.api.entity.GoodAttribute;
import com.nishop.api.entity.GoodAttributeExample;
import com.nishop.api.form.BaseRequestForm;
import com.nishop.api.util.Identity;
import com.nishop.service.dao.AttributeCategoryMapper;
import com.nishop.service.dao.GoodAttributeMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author 李建成
 *
 */
@Service
public class AttrsServiceImpl implements IAttrsService {

    @Autowired
    private AttributeCategoryMapper attributeCategoryMapper;
    @Autowired
    private GoodAttributeMapper goodAttributeMapper;

    @Override
    public List<AttributeCategory> queryList(BaseRequestForm requestForm) {


        AttributeCategoryExample example = getAttributeCategoryExample();

        List<AttributeCategory> attributeCategories = attributeCategoryMapper.selectByExample(example);

        this.builderCateg(attributeCategories);

        return attributeCategories;
    }

    private void builderCateg(List<AttributeCategory> attributeCategories) {
        if (CollectionUtils.isNotEmpty(attributeCategories)) {
            attributeCategories.forEach(new Transfer());
        }
    }

    @Override
    public boolean editSub(GoodAttribute attribute) {
        return goodAttributeMapper.updateByPrimaryKey(attribute) > 0;
    }

    @Override
    public boolean delSub(GoodAttribute attribute) {
        return goodAttributeMapper.deleteByPrimaryKey(attribute.getId()) > 0;
    }

    @Override
    public boolean addSub(GoodAttribute attribute) {
        attribute.setId(Identity.uuidWithoutHyphen());
        return goodAttributeMapper.insertSelective(attribute) > 0;
    }

    private AttributeCategoryExample getAttributeCategoryExample() {
        return new AttributeCategoryExample();
    }

    private GoodAttributeExample goodAttributeExample() {
        return new GoodAttributeExample();
    }

    @Override
    public long queryCount() {

        AttributeCategoryExample example = getAttributeCategoryExample();

        return attributeCategoryMapper.countByExample(example);
    }

    @Override
    public Boolean edit(AttributeCategory category) {
        return attributeCategoryMapper.updateByPrimaryKeySelective(category) > 0;
    }

    @Override
    public Boolean add(AttributeCategory category) {
        category.setId(Identity.uuidWithoutHyphen());
        return attributeCategoryMapper.insert(category) > 0;
    }

    @Override
    public Boolean del(AttributeCategory category) {

        String id = category.getId();

        if (Objects.isNull(id)) {
            return false;
        }
        GoodAttributeExample attributeCategoryExample = this.goodAttributeExample();

        attributeCategoryExample.createCriteria().andAttributeCategoryIdEqualTo(id);

        return this.del(id, attributeCategoryExample);
    }

    /**
     * 删除属性和子属性
     *
     * @param id
     * @param attributeCategoryExample
     * @return boolean
     */
    private boolean del(String id, GoodAttributeExample attributeCategoryExample) {
        return goodAttributeMapper.deleteByExample(attributeCategoryExample)!=-1
                && attributeCategoryMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 实现 Consumer构建 attributeCategories
     */
    class Transfer implements Consumer<AttributeCategory> {
        @Override
        public void accept(AttributeCategory attributeCategory) {
            String cateId = attributeCategory.getId();
            GoodAttributeExample example = new GoodAttributeExample();
            example.createCriteria().andAttributeCategoryIdEqualTo(cateId);
            List<GoodAttribute> goodAttributes = goodAttributeMapper.selectByExample(example);
            attributeCategory.setGoodAttributes(goodAttributes);
        }
    }
}
