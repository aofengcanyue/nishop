/**
 *
 */
package com.nishop.service;

import com.nishop.api.IOrdersService;
import com.nishop.api.entity.Order;
import com.nishop.api.entity.OrderExample;
import com.nishop.api.entity.OrderGoods;
import com.nishop.api.entity.OrderGoodsExample;
import com.nishop.api.form.BaseRequestForm;
import com.nishop.api.util.Identity;
import com.nishop.service.dao.OrderGoodsMapper;
import com.nishop.service.dao.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author 李建成
 *
 */
@Service
public class OrdersServiceImpl implements IOrdersService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderGoodsMapper orderGoodsMapper;

    @Override
    public List<Order> queryList(BaseRequestForm requestForm) {


        List<Order> orders = orderMapper.getOrder(new Order());

        return orders;
    }




    private OrderExample getOrderExample() {
        OrderExample example = new OrderExample();
        return example;
    }

    @Override
    public long queryCount() {

        OrderExample example = getOrderExample();

        long count = orderMapper.countByExample(example);

        return count;
    }

    @Override
    public Boolean edit(Order category) {
        return orderMapper.updateByPrimaryKeySelective(category) > 0;
    }

    @Override
    public Boolean add(Order category) {
        category.setId(Identity.uuidWithoutHyphen());
        return orderMapper.insert(category) > 0;
    }

    @Override
    public Boolean del(Order category) {

        String id = category.getId();

        if (Objects.isNull(id)) {
            return false;
        }

        return orderMapper.deleteByPrimaryKey(id) > 0;
    }
}
