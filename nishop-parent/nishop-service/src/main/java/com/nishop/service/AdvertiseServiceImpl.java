/**
 *
 */
package com.nishop.service;

import com.nishop.api.IAdvertiseService;
import com.nishop.api.IAdvertiseService;
import com.nishop.api.entity.Advertise;
import com.nishop.api.entity.AdvertiseExample;
import com.nishop.api.form.BaseRequestForm;
import com.nishop.api.util.Identity;
import com.nishop.service.dao.AdvertiseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author 李建成
 *
 */
@Service
public class AdvertiseServiceImpl implements IAdvertiseService {

    @Autowired
    private AdvertiseMapper AdvertiseMapper;

    @Override
    public List<Advertise> queryList(BaseRequestForm requestForm) {


        AdvertiseExample example = getAdvertiseExample();

        List<Advertise> attributeCategories = AdvertiseMapper.selectByExampleWithBLOBs(example);

        return attributeCategories;
    }

    private AdvertiseExample getAdvertiseExample() {
        AdvertiseExample example = new AdvertiseExample();
        return example;
    }

    @Override
    public long queryCount() {

        AdvertiseExample example = getAdvertiseExample();

        long count = AdvertiseMapper.countByExample(example);

        return count;
    }

    @Override
    public Boolean edit(Advertise category) {
        return AdvertiseMapper.updateByPrimaryKeyWithBLOBs(category) > 0;
    }

    @Override
    public Boolean add(Advertise category) {
        category.setId(Identity.uuidWithoutHyphen());
        return AdvertiseMapper.insertSelective(category) > 0;
    }

    @Override
    public Boolean del(Advertise category) {

        String id = category.getId();

        if (Objects.isNull(id)) {
            return false;
        }

        return AdvertiseMapper.deleteByPrimaryKey(id) > 0;
    }
}
