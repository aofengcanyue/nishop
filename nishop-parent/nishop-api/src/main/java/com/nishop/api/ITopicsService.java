package com.nishop.api;

import com.nishop.api.entity.Topic;
import com.nishop.api.form.TopicsRequestForm;

import java.util.ArrayList;
import java.util.List;

public interface ITopicsService extends BaseService<Topic> {

    default List<Topic> queryTopicList(TopicsRequestForm requestForm) {
        return new ArrayList<>();
    }
}
