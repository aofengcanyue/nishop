package com.nishop.api;

import com.nishop.api.form.BaseRequestForm;

import java.util.ArrayList;
import java.util.List;

public interface BaseService<T> {


    default List<T> queryList(BaseRequestForm requestForm) {
        return new ArrayList<>();
    }

    default long queryCount() {
        return 0;
    }

    default Boolean edit(T endity) {
        return false;
    }

    default Boolean add(T endity) {
        return false;
    }

    default Boolean del(T endity) {
        return false;
    }
}
