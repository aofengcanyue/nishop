package com.nishop.api.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NiShopConfig {

	@Value("${server.prefix:/opt/}")
	public String serverPrefix;

	@Value("${nishop.goods.image.path:/opt/nishop/image/}")
	public String goodsImagePath;

}
