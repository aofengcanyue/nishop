package com.nishop.api.util;

import java.util.UUID;

import com.nishop.api.common.CharacterConstant;

public class Identity {

	public static Integer createId() {
		StringBuffer buffer = new StringBuffer(6);
		for (int i = 0; i <= 6; i++) {
			buffer.append((int) (Math.random() * 10));
		}
		return Integer.valueOf(buffer.toString());
	}

	/**
	 * uuid 不包含连接符
	 * 
	 * @return
	 */
	public static String uuidWithoutHyphen() {
		return UUID.randomUUID().toString().replaceAll(CharacterConstant.CHARACTER_HYPHEN, "");
	}
}
