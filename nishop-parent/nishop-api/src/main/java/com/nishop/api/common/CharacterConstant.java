/**
 * 
 */
package com.nishop.api.common;

/**
 * @author liuwenlai
 *
 */
public class CharacterConstant {

	/**
	 * 英文逗号
	 */
	public final static String CHARACTER_COMMA_EN = ",";

	/**
	 * 英文冒号
	 */
	public final static String CHARACTER_COLON_EN = ":";

	/**
	 * 横杠连接号
	 */
	public final static String CHARACTER_HYPHEN = "-";

}
