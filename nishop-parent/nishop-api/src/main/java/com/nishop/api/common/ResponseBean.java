package com.nishop.api.common;

import java.io.Serializable;

public class ResponseBean<T> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public T result;

	public boolean success;

	public String msg;

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ResponseBean() {

	}
	public ResponseBean(T result, boolean success, String msg) {
		super();
		this.result = result;
		this.success = success;
		this.msg = msg;
	}
	public ResponseBean(T result , String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public static <T> ResponseBean<T>  success(T t, String msg) {
		return new ResponseBean(t, true, msg);
	}

	public static <T> ResponseBean<T>  fail(T t, String msg) {
		return new ResponseBean(t, false, msg);
	}
	
}
