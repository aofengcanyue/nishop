/**
 * 
 */
package com.nishop.api.form;

/**
 * @author liuwenlai
 *
 */
public class GoodsRequestForm extends BaseRequestForm {

	private static final long serialVersionUID = 1L;

	private String goodsName;
	
	private String attributeId;
	
	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

}
