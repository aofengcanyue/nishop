/**
 * 
 */
package com.nishop.api;

import com.nishop.api.entity.AttributeCategory;
import com.nishop.api.entity.GoodAttribute;

/**
 * @author 李建成
 *
 */
public interface IAttrsService extends BaseService<AttributeCategory>{

    boolean editSub(GoodAttribute attribute);

    boolean delSub(GoodAttribute attribute);

    boolean addSub(GoodAttribute attribute);
}
