/**
 * 
 */
package com.nishop.api;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.nishop.api.entity.Goods;
import com.nishop.api.form.GoodsRequestForm;
import com.nishop.api.layui.entity.TreeNode;

/**
 * @author liuwenlai
 *
 */
public interface IGoodsService {

	List<Goods> queryGoodsList(GoodsRequestForm goodsRequestForm);

	long queryGoodsCount(GoodsRequestForm goodsRequestForm);

	String uploadGoodsImages(CommonsMultipartFile file) throws IOException;

	boolean addGoods(Goods goods);

	boolean editGoods(Goods goods);

	boolean delGoods(Goods goods);

	List<TreeNode> queryAttrTreeData(GoodsRequestForm goodsRequestForm);

}
