/**
 *
 */
package com.nishop.api;

import com.nishop.api.entity.TopicCategory;

/**
 * @author 李建成
 *
 */
public interface ITopicCategoryService extends BaseService<TopicCategory> {


}
