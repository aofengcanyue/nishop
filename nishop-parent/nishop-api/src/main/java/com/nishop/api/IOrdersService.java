/**
 *
 */
package com.nishop.api;

import com.nishop.api.entity.Order;

/**
 * @author 李建成
 *
 */
public interface IOrdersService extends BaseService<Order> {


}
