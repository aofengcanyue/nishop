/**
 * 
 */
package com.nishop.api.common;

import java.io.Serializable;
import java.util.List;

/**
 * @author liuwenlai
 *
 */
public class LayuiTableResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private int code = 0;

	private String msg;

	private long count;

	private List<T> data;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

}
