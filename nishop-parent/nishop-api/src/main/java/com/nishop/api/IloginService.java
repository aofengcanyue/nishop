package com.nishop.api;

import com.nishop.api.entity.AdminUser;

public interface IloginService {

	AdminUser loginValid(AdminUser user);
	
}
