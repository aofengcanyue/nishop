# nishop

#### 介绍
电商后台

#### 软件架构
springmvc mybatis mysql jsrender layui


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md

#### 参考地址（http://demo25.crmeb.net 账号：demo 密码：crmeb.com）

#### 需求 (功能完毕，标记 --ok) 
   
1. 整体改进
   1. 需要增加二级菜单，
      1. 一级 (商品)，二级（商品管理 ，商品分类）
         1. 商品管理( 增删改查)
         2. 商品分类( 增删改查)
      2. 一级 （订单），二级（订单管理，售后服务）
         1. 订单管理( 分类查询，订单详情，订单批注，订单记录)
         2. 售后服务( 评论管理，查看)
      3. 一级（会员），二级（会员管理）
         1. 会员福利（会员列表展示，发送优惠券，）
      4. 一级（营销），二级（优惠券）
         1. 优惠券管理（增删改查，）
      5. 一级 （内容） 二级（文章分类，文章管理）
      6. 首页轮播增删改查（图片地址回显测试）


   2. 头部导航通知
    待发货 53个
    库存预警 16个
    新评论 4个
    
2. 商品管理
   1. 商品的信息，需要关联在二级分类上面
   2. 新增编辑商品，同上关联二级分类， 删除商品，编辑商品
   3. 新增编辑商品，支持主图，商品的banner 多图片上传
   4. 线上回显，要测试一下，支持是否热门商品（对 SELECT * FROM `nideshop_goods_gallery` 这张表进行删除和新增关联商品的ID）
   5. 加状态是否是新的商品  （is_new	是1 ）
   6. 加状态是否是人气推荐的商品（is_hot	是1 ）
   	
3. 商品分类
   1. 分类管理支持，编辑分类的banner 和 主图的修改
   2. 分类管理的新增，编辑，级别的选择
4. 订单管理
   1. 订单的，高级查询，订单详情，订单批注，订单记录
5. 售后服务
   1. 评论的查看
   2. 评论回复   
6. 会员福利
   1. 会员列表展示
   2. 发送优惠券
7. 优惠券
   1. 增删改查
   2. 优惠券的发布，撤销发布，有效期设置,关联会员列表
8.内容管理
   1. 文章分类
   2. 文章管理
9.小程序配置




  商户配置
  可定制化插件 GaeaEditor 在线    
  GoJS	https://gojs.net/latest/samples/flowchart.html	推荐使用
  前端可视化建模技术概览：http://leungwensen.github.io/blog/2015/frontend-visual-modeling.html
  JS流程图库：https://blog.csdn.net/gdp12315_gu/article/details/54310854
  页面可视化搭建工具前生今世：https://github.com/CntChen/cntchen.github.io/issues/15
  前端服务化——页面搭建工具的死与生：https://www.cnblogs.com/sskyy/p/6496287.html
  跟我们需求比较类似的一个库：https://github.com/ascoders/gaea-editor （https://gaeajs.github.io/gaea-site/）
  
 gitHub 地址
  Web  ：https://github.com/page-pipepline/pipeline-editor
  文章 ：
  https://zhuanlan.zhihu.com/p/37171897
  
  开源
  https://github.com/page-pipepline/pipeline-document/wiki/Pipeline-%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2



2. 项目测试
 1. 模块测试  
 2. 风险测试 




3. 项目上线




1. 模块测试
  
  1.商品首页 (接口地址， indexAction  ( cannel  )
  2.
